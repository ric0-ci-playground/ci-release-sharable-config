# Playground for "semantic-release sharable config"

**The `CI_JOB_TOKEN` doesnt seem to work, trying project or group access-token instead **

Goal: Create a sharable config, which is a npm-package.

Tasks:

- [ ] Create `.gitlabci.yml`
- [ ] Publish the config as a npm package to Gitlab
- [ ] Use the config in the [ci-release-playground](https://gitlab.com/ric0-ci-playground/ci-release-playground)